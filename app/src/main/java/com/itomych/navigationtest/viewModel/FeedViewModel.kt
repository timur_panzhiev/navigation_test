package com.itomych.navigationtest.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.itomych.navigationtest.datasource.factory.FeedDataFactory
import com.itomych.navigationtest.repository.model.Article
import com.itomych.navigationtest.utils.NetworkState

import java.util.concurrent.Executors

class FeedViewModel : ViewModel() {

    var initialNetworkState: LiveData<NetworkState>? = null
    var paginatedNetworkState: LiveData<NetworkState>? = null
    var pagedListArticleLiveData: LiveData<PagedList<Article>>? = null
    var feedDataFactory: FeedDataFactory? = null

    init {
        init()
    }

    private fun init() {
        val executor = Executors.newFixedThreadPool(5)
        feedDataFactory = FeedDataFactory()
        initialNetworkState = Transformations.switchMap(feedDataFactory!!.dataSourceLiveData) { source -> source.initialNetworkState }
        paginatedNetworkState = Transformations.switchMap(feedDataFactory!!.dataSourceLiveData) { source -> source.paginatedNetworkState }

        val pagedListConfig = PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(15)
                .setPageSize(10).build()

        pagedListArticleLiveData = LivePagedListBuilder(feedDataFactory!!, pagedListConfig)
                .setFetchExecutor(executor)
                .build()
    }

    fun retry() {
        feedDataFactory!!.feedDataSource!!.retryPagination()
    }

    fun retryInitial(){

    }
}
