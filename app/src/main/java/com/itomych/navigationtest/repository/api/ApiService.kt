package com.itomych.navigationtest.repository.api

import com.itomych.navigationtest.repository.model.Feed
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/v2/everything")
    fun getFeed(@Query("q") q: String,
                @Query("apiKey") apiKey: String,
                @Query("page") page: Long,
                @Query("pageSize") pageSize: Int): Call<Feed>
}
