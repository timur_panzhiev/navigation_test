package com.itomych.navigationtest.datasource

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.itomych.navigationtest.repository.api.API_KEY
import com.itomych.navigationtest.repository.api.ApiProvider
import com.itomych.navigationtest.repository.api.ApiService
import com.itomych.navigationtest.repository.api.MOVIES_PATH
import com.itomych.navigationtest.repository.model.Article
import com.itomych.navigationtest.repository.model.Feed
import com.itomych.navigationtest.utils.NetworkState
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FeedDataSource : PageKeyedDataSource<Int, Article>() {

    val paginatedNetworkState: MutableLiveData<NetworkState> = MutableLiveData()
    val initialNetworkState: MutableLiveData<NetworkState> = MutableLiveData()
    val api: ApiService = ApiProvider().apiService
    // For Retry
    private var params: PageKeyedDataSource.LoadParams<Int>? = null
    private var callback: PageKeyedDataSource.LoadCallback<Int, Article>? = null

    override fun loadInitial(params: PageKeyedDataSource.LoadInitialParams<Int>, callback: PageKeyedDataSource.LoadInitialCallback<Int, Article>) {

        initialNetworkState.postValue(NetworkState.LOADING)
        paginatedNetworkState.postValue(NetworkState.LOADING)

        api.getFeed(MOVIES_PATH, API_KEY, 1, params.requestedLoadSize)
                .enqueue(object : Callback<Feed> {

                    override fun onResponse(call: Call<Feed>, response: Response<Feed>) {
                        if (response.isSuccessful) {
                            onSuccessInitial(callback, response)
                        } else {
                            onErrorInitial(response.message())
                        }
                    }

                    override fun onFailure(call: Call<Feed>, t: Throwable) {
                        onErrorInitial(t.message!!)
                    }
                })
    }

    override fun loadBefore(params: PageKeyedDataSource.LoadParams<Int>, callback: PageKeyedDataSource.LoadCallback<Int, Article>) {
    }

    override fun loadAfter(params: PageKeyedDataSource.LoadParams<Int>, callback: PageKeyedDataSource.LoadCallback<Int, Article>) {

        paginatedNetworkState.postValue(NetworkState.LOADING)

        this.params = params
        this.callback = callback

        api.getFeed(MOVIES_PATH, API_KEY, params.key.toLong(), params.requestedLoadSize)
                .enqueue(object : Callback<Feed> {
                    override fun onResponse(call: Call<Feed>, response: Response<Feed>) {
                        if (response.isSuccessful) {
                            onSuccessLoadMore(params, callback, response)
                        } else
                            onErrorLoadMore(response.message())
                    }

                    override fun onFailure(call: Call<Feed>, t: Throwable) {
                        onErrorLoadMore(t.message!!)
                    }
                })
    }

    fun onErrorInitial(msg: String) {
        initialNetworkState.postValue(NetworkState(NetworkState.Status.FAILED, msg))
    }

    fun onSuccessInitial(callback: PageKeyedDataSource.LoadInitialCallback<Int, Article>, response: Response<Feed>) {
        callback.onResult(response.body()!!.articles!!, null, 2)
        initialNetworkState.postValue(NetworkState.LOADED)
    }

    fun onErrorLoadMore(msg: String) {
        paginatedNetworkState.postValue(NetworkState(NetworkState.Status.FAILED, msg))
    }

    fun onSuccessLoadMore(params: PageKeyedDataSource.LoadParams<Int>, callback: PageKeyedDataSource.LoadCallback<Int, Article>, response: Response<Feed>) {
        val nextKey = if (params.key == response.body()!!.totalResults) null else params.key + 1
        callback.onResult(response.body()!!.articles!!, nextKey)
        paginatedNetworkState.postValue(NetworkState.LOADED)
    }

    fun retryPagination() {
        loadAfter(params!!, callback!!)
    }

    companion object {
        private val TAG = FeedDataSource::class.java.simpleName
    }
}
