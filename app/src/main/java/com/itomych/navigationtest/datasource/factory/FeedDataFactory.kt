package com.itomych.navigationtest.datasource.factory

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.itomych.navigationtest.datasource.FeedDataSource
import com.itomych.navigationtest.repository.model.Article

class FeedDataFactory : DataSource.Factory<Int, Article>() {

    val dataSourceLiveData: MutableLiveData<FeedDataSource> = MutableLiveData()
    var feedDataSource: FeedDataSource? = null

    override fun create(): DataSource<Int, Article> {
        feedDataSource = FeedDataSource()
        dataSourceLiveData.postValue(feedDataSource)
        return feedDataSource!!
    }
}