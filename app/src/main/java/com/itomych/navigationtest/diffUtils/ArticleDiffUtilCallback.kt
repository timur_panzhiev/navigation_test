package com.itomych.navigationtest.diffUtils

import androidx.recyclerview.widget.DiffUtil
import com.itomych.navigationtest.repository.model.Article

class ArticleDiffUtilCallback : DiffUtil.ItemCallback<Article>() {

    override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean {
        return oldItem.publishedAt.equals(newItem.publishedAt)
                && oldItem.title.equals(newItem.title)
    }

    override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
        return oldItem.publishedAt.equals(newItem.publishedAt)
                && oldItem.title.equals(newItem.title)
    }
}