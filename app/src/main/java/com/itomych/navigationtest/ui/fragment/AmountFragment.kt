package com.itomych.navigationtest.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itomych.navigationtest.R
import com.itomych.navigationtest.utils.hideKeyboard
import kotlinx.android.synthetic.main.fragment_amount.*

class AmountFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_amount, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        tv_recipient.text = getString(R.string.to_recipient, arguments?.get("recipient").toString())
    }

    private fun init() {
        btn_send.setOnClickListener {
            hideKeyboard(it)
            navController!!.navigate(R.id.action_to_complete)
        }
        btn_back.setOnClickListener { navController!!.popBackStack() }
    }
}