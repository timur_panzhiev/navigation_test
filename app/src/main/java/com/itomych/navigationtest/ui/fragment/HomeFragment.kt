package com.itomych.navigationtest.ui.fragment

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.core.app.NotificationCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.itomych.navigationtest.R
import kotlinx.android.synthetic.main.fragment_dashboard.*


class HomeFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        btn_send_money.setOnClickListener { navController!!.navigate(R.id.action_to_recipient) }
        btn_balance.setOnClickListener { navController!!.navigate(R.id.action_to_balance) }
        btn_notify.setOnClickListener { v -> sendNotification(v) }
    }

    private fun sendNotification(v: View) {
        val args = Bundle()
        args.putString("from", "From Notification!")
        val channelId = "default_channel_id"

        val pendingIntent = Navigation.findNavController(v)
            .createDeepLink()
            .setDestination(R.id.deepLinkFragment)
            .setArguments(args)
            .createPendingIntent()

        val notificationManager = context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val builder = NotificationCompat.Builder(context!!, channelId)
            .setContentTitle("Navigation")
            .setContentText("Deep link to Android")
            .setSmallIcon(R.drawable.ic_deep)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(
                NotificationChannel(
                    channelId, "Deep Links", NotificationManager.IMPORTANCE_HIGH
                )
            )
        }

        notificationManager.notify(0, builder.build())
    }
}