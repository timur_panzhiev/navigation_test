package com.itomych.navigationtest.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.itomych.navigationtest.R
import com.itomych.navigationtest.diffUtils.ArticleDiffUtilCallback
import com.itomych.navigationtest.ui.fragment.adapter.FeedListAdapter
import com.itomych.navigationtest.viewModel.FeedViewModel
import kotlinx.android.synthetic.main.fragment_feed.*
import com.itomych.navigationtest.utils.NetworkState

class FeedFragment : BaseFragment(), FeedListAdapter.Callback {

    companion object {
        val TAG = FeedFragment::class.java.simpleName
    }

    var feedViewModel: FeedViewModel? = null
    private lateinit var adapter: FeedListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_feed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()

        feedViewModel = ViewModelProviders.of(this).get(FeedViewModel::class.java)

        adapter = FeedListAdapter(ArticleDiffUtilCallback(), this)
        feedViewModel!!.pagedListArticleLiveData!!.observe(this, Observer { pagedList -> adapter.submitList(pagedList) })
        feedViewModel!!.initialNetworkState!!.observe(this, Observer(this::onInitialLoading))
        feedViewModel!!.paginatedNetworkState!!.observe(this, Observer { networkState -> adapter.setNetworkState(networkState) })

        rv_feed.adapter = adapter
    }

    private fun init() {
        rv_feed.layoutManager = LinearLayoutManager(context)
        rv_feed.setHasFixedSize(true)
        btn_retry_initial.setOnClickListener { invalidate() }
    }

    override fun onRetry() {
        feedViewModel!!.retry()
    }

    private fun onInitialLoading(state: NetworkState) {
        when (state.status) {
            NetworkState.Status.SUCCESS -> {
                swipe_refresh_layout.visibility = View.VISIBLE
            }
            NetworkState.Status.FAILED -> {
                swipe_refresh_layout.visibility = View.GONE
                ll_try_again.visibility = View.VISIBLE
            }
            NetworkState.Status.RUNNING -> {
                ll_try_again.visibility = View.GONE
                swipe_refresh_layout.visibility = View.GONE
            }
        }
    }

    private fun invalidate() {
        feedViewModel!!.feedDataFactory!!.feedDataSource!!.invalidate()
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu?.clear()
        super.onPrepareOptionsMenu(menu)
    }
}