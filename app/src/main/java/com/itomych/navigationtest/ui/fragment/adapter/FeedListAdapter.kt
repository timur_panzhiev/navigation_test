package com.itomych.navigationtest.ui.fragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.itomych.navigationtest.R
import com.itomych.navigationtest.repository.model.Article
import com.itomych.navigationtest.utils.NetworkState
import kotlinx.android.synthetic.main.item_feed.view.*
import kotlinx.android.synthetic.main.item_load_more.view.*

class FeedListAdapter(diffCallback: DiffUtil.ItemCallback<Article>, val callback: Callback) :
        PagedListAdapter<Article, RecyclerView.ViewHolder>(diffCallback) {

    private var networkState: NetworkState? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v: View = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        when (viewType) {
            R.layout.item_feed -> return ArticleItemViewHolder(v)
            R.layout.item_load_more -> return NetworkStateItemViewHolder(v)
        }
        return ArticleItemViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ArticleItemViewHolder) {
            holder.bind(getItem(position))
        } else {
            (holder as NetworkStateItemViewHolder).bind(networkState)
        }
    }

    private fun hasExtraRow(): Boolean {
        return networkState != null && networkState != NetworkState.LOADED
    }

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1) R.layout.item_load_more else R.layout.item_feed
    }

    fun setNetworkState(newNetworkState: NetworkState) {
        val previousState = this.networkState
        val previousExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val newExtraRow = hasExtraRow()
        if (previousExtraRow != newExtraRow) {
            if (previousExtraRow) {
                notifyItemRemoved(itemCount)
            } else {
                notifyItemInserted(itemCount)
            }
        } else if (newExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    inner class ArticleItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(article: Article?) {

            val context: Context = itemView.context

            article?.let {
                itemView.tv_title.text = it.title
                itemView.tv_desc.text = it.description
                itemView.tv_time.text = it.publishedAt

                Glide.with(context)
                        .load(it.urlToImage)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .apply(
                                RequestOptions()
                                        .centerCrop()
//                        .error(Utils.getTextDrawable(context, coin.symbol))
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        )
                        .into(itemView.iv_feed)
            }
        }
    }


    inner class NetworkStateItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(networkState: NetworkState?) {

            networkState?.let { it ->

                itemView.visibility = View.VISIBLE

                when (it.status) {
                    NetworkState.Status.RUNNING -> {
                        itemView.pb_paging.visibility = View.VISIBLE
                        itemView.iv_retry.visibility = View.GONE
                    }
                    NetworkState.Status.FAILED -> {
                        itemView.pb_paging.visibility = View.GONE
                        itemView.iv_retry.visibility = View.VISIBLE
                        itemView.iv_retry.setOnClickListener { callback.onRetry() }
                    }
                    else -> {
                        itemView.visibility = View.GONE
                    }
                }
            }
        }
    }

    interface Callback {
        fun onRetry()
    }
}
