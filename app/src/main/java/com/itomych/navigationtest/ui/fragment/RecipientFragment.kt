package com.itomych.navigationtest.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itomych.navigationtest.R
import kotlinx.android.synthetic.main.fragment_recipient.*

class RecipientFragment : BaseFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_recipient, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        btn_next.setOnClickListener {
            val b = Bundle()
            b.putString("recipient", et_recipient.text.toString())
            navController!!.navigate(R.id.action_to_amount, b)
        }
        btn_back.setOnClickListener { navController!!.popBackStack() }
    }
}