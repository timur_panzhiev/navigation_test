package com.itomych.navigationtest.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itomych.navigationtest.R
import kotlinx.android.synthetic.main.fragment_deep_link.*

class DeepLinkFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_deep_link, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            tv_deepLink_from.text = it.getString("from")
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = DeepLinkFragment()
    }
}